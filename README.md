# README #

This contains the source files to go with the video training on youtube.

* [Part 1](https://www.youtube.com/watch?v=gPwKXY_l0bE)
* [Part 2](https://www.youtube.com/watch?v=MW_G81xlISo)
* [Part 3](https://www.youtube.com/watch?v=5Uif9C1DeQM)
* [Part 4](https://www.youtube.com/watch?v=ELaIUm6nF68)
* [Part 5](https://www.youtube.com/watch?v=3VwSscf5_gs)


### Swyx NIght Service ###

* This script allows users to set / unset night service manually
* Version 1.0.1


